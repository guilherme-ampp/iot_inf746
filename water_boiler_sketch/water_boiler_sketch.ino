#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

#include <Servo.h>
#include <Wire.h>
#include <TH02_dev.h>
#include "rgb_lcd.h"

// overriding aws_iot_config.h
#undef AWS_IOT_MQTT_RX_BUF_LEN
#define AWS_IOT_MQTT_RX_BUF_LEN 1024

#undef AWS_IOT_MQTT_TX_BUF_LEN
#define AWS_IOT_MQTT_TX_BUF_LEN 1024

// STATE MACHINE
#define DEBUG 0
#define OFF 0
#define ON 1
#define STAND_BY 2

int state = OFF;

int SERVO_POSITION_GAS = 0;
int SERVO_POSITION_ELECTRIC = 90;

const int pinButton = 0;
const int pinRelay = 5;
const int pinServo = 6;
Servo myservo;

rgb_lcd lcd;

String command;
float water_temperature = 0;
float moisture_level = 0;
float gas_heating_price = 0;
float electric_heating_price = 0;
float target_temperature = 0;
boolean local_button = false;
boolean remote_button = false;
char heating_system_state[2] = "O";
boolean simulation = false;        // entering water temperature or moisture level manually triggers simulation mode and ignores the sensors.

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 400

static char certDirectory[PATH_MAX + 1] = "/home/root/certs";
static char HostAddress[255] = AWS_IOT_MQTT_HOST;
static uint32_t port = AWS_IOT_MQTT_PORT;
static uint8_t numPubs = 5;

AWS_IoT_Client mqttClient;
IoT_Error_t rc = FAILURE;
int32_t i = 0;

char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

jsonStruct_t faucetActuator;
jsonStruct_t gasPriceHandler;
jsonStruct_t electricPriceHandler;
jsonStruct_t temperatureHandler;
jsonStruct_t targetTemperatureHandler;
jsonStruct_t heatingSystemStateHandler;

char rootCA[PATH_MAX + 1];
char clientCRT[PATH_MAX + 1];
char clientKey[PATH_MAX + 1];
char CurrentWD[PATH_MAX + 1];

float calculate_gas_heating_price() {
  /**
   * Calculate gas heating cost accounting for efficiency
   */
   return gas_heating_price * (1.0 - (moisture_level/1000.0) * 8.0);
}

boolean isFaucetOn() {
  return local_button || remote_button;
}

void debugStateMachine() {
  if (DEBUG) {
    Serial.println("-------------------------");
    Serial.print("Water temperature: ");
    Serial.print(water_temperature);
    Serial.println(" C");

    Serial.print("Target temperature: ");
    Serial.print(target_temperature);
    Serial.println(" C");

    Serial.print("Moisture: ");
    Serial.println(moisture_level);

    Serial.print("Gas cost: ");
    Serial.print(calculate_gas_heating_price());
    Serial.println(" kWh");

    Serial.print("Electricity cost: ");
    Serial.print(electric_heating_price);
    Serial.println(" kWh");

    Serial.print("Remote button: ");
    Serial.println(remote_button);

    Serial.print("Local button: ");
    Serial.println(isFaucetOn());

    Serial.print("Heating: ");
    Serial.println(heating_system_state[0]);

    Serial.print("STATE: ");
    if (state == OFF)
      Serial.println("OFF");
    if (state == ON)
      Serial.println("ON");
    if (state == STAND_BY)
      Serial.println("STAND_BY");
    Serial.println("-------------------------");
  }
}

bool readFromSerial() {
  /**
   * Reads a string from serial monitor input
   */
  bool _read = false;
  while (Serial.available()) {
    command = Serial.readString();
    _read = true;
  }
  return _read;
}

void readFromSensors() {
  if (!simulation) {
    water_temperature = TH02.ReadTemperature();
    moisture_level = TH02.ReadHumidity();
  }
}

void toggleFaucet() {
  local_button = !local_button;
  if (isFaucetOn()) {
    toggleRelayOn();  // faucet is on
  } else {
    toggleRelayOff();  // faucet is off
  }
}

void printInformationToLCD() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(heating_system_state[0]);
  lcd.setCursor(4, 0);
  lcd.print(water_temperature);
  lcd.print(" C");
  lcd.setCursor(4, 1);
  lcd.print(target_temperature);
  lcd.print(" C");
}

void moveServo() {
  if (heating_system_state[0] == 'G') {
    myservo.write(SERVO_POSITION_GAS);
  }
  else if (heating_system_state[0] == 'E') {
    myservo.write(SERVO_POSITION_ELECTRIC);
  }
}

void toggleRelayOn() {
  digitalWrite(pinRelay, HIGH);
}

void toggleRelayOff() {
  digitalWrite(pinRelay, LOW);
}

void processCommand(String input) {
  /**
   * Processes the input command into the state machine.
   */
  Serial.print("Command: ");
  Serial.println(input);
  char action = input[0];

  int len = input.length() - 1;
  char * arg = new char[len];
  input.substring(2).toCharArray(arg, len);

  float value = atof(arg);

  if(action == 'G') {
    gas_heating_price = value;
    Serial.print("Set gas heating price: ");
    Serial.println(gas_heating_price);
  }
  else if(action == 'E') {
    electric_heating_price = value;
    Serial.print("Set electric heating price: ");
    Serial.println(electric_heating_price);
  }
  else if(action == 'T') {
    target_temperature = value;
    Serial.print("Set target temperature to: ");
    Serial.println(target_temperature);
  }
  else if(action == 'R') {
    remote_button = value;
    Serial.print("Toggle remote button: ");
    Serial.println(remote_button);
  }
  else if(action == 'L') {
    local_button = value;
    Serial.print("Toggle local button: ");
    Serial.println(local_button);
  }
  else if(action == 'W') {
    water_temperature = value;
    simulation = true;
    Serial.print("Set water temperature (simulation): ");
    Serial.println(water_temperature);
  }
  else if(action == 'M') {
    moisture_level = value;
    simulation = true;
    Serial.print("Set moisture level (simulation): ");
    Serial.println(moisture_level);
  }
}

void state_machine() {
  /**
   * State machine for the water heating system
   * 
   * STATE OFF: Faucet is closed
   * STATE ON: Keeps computing electric and gas heating prices to switch to the cheaper
   * STATE STAND_BY: Keeps reading current and target water temperature
   */
   if (state == OFF) {
     heating_system_state[0] = 'O';
     moveServo();

     if (isFaucetOn()) {
       toggleRelayOn();  // faucet is on
       state = STAND_BY;
     }
   }
   
   else if (state == ON) {
     float gas_price = calculate_gas_heating_price();
     if (electric_heating_price < gas_price) {
       heating_system_state[0] = 'E';
       moveServo();
     }
     else {
       heating_system_state[0] = 'G';
       moveServo();
     }
     
     if (water_temperature >= target_temperature) {
       toggleRelayOff();
       state = STAND_BY;   // traverse to STAND_BY
     }
     if (!isFaucetOn()) {
       toggleRelayOff();  // faucet is on
       state = OFF;       // faucet is off, traverse to OFF
     }
   }
   
   else if (state == STAND_BY) {
     if (!isFaucetOn()) {
       state = OFF; 
     }
     else if (water_temperature < target_temperature) {
       state = ON;
     }
   }
}

void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                const char *pReceivedJsonDocument, void *pContextData) {
  IOT_UNUSED(pThingName);
  IOT_UNUSED(action);
  IOT_UNUSED(pReceivedJsonDocument);
  IOT_UNUSED(pContextData);

  if(SHADOW_ACK_TIMEOUT == status) {
    Serial.println("Update Timeout--");
  } else if(SHADOW_ACK_REJECTED == status) {
    Serial.println("Update RejectedXX");
  } else if(SHADOW_ACK_ACCEPTED == status) {
    Serial.println("Update Accepted !!");
  }
}

void actuate_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  IOT_UNUSED(pJsonString);
  IOT_UNUSED(JsonStringDataLen);

  Serial.print("Delta for key - ");
  Serial.println(pContext->pKey);

  if(pContext != NULL) {
    if (pContext->pKey == "remote_button") {
      Serial.print("Delta - Faucet remote button state changed to ");
      Serial.println(*(bool *) (pContext->pData));
    } else {
      Serial.print("Delta - ");
      Serial.print(pContext->pKey);
      Serial.print(" changed to ");
      Serial.println(*(float *) (pContext->pData));
    }
  }
}

void setup() {
  pinMode(pinRelay,OUTPUT);
  pinMode(pinButton,INPUT);
  myservo.attach(pinServo);
  SERVO_POSITION_GAS = myservo.read();
  TH02.begin();
  // LCD's number of columns and rows
  lcd.begin(16, 2);
  Serial.begin(9600);

  attachInterrupt(pinButton, toggleFaucet, RISING);

  faucetActuator.cb = actuate_Callback;
  faucetActuator.pKey = "remote_button";
  faucetActuator.pData = &remote_button;
  faucetActuator.type = SHADOW_JSON_BOOL;

  gasPriceHandler.cb = actuate_Callback;
  gasPriceHandler.pKey = "gas_cost";
  gasPriceHandler.pData = &gas_heating_price;
  gasPriceHandler.type = SHADOW_JSON_FLOAT;

  electricPriceHandler.cb = actuate_Callback;
  electricPriceHandler.pKey = "electric_cost";
  electricPriceHandler.pData = &electric_heating_price;
  electricPriceHandler.type = SHADOW_JSON_FLOAT;
 
  temperatureHandler.cb = NULL;
  temperatureHandler.pKey = "water_temperature";
  temperatureHandler.pData = &water_temperature;
  temperatureHandler.type = SHADOW_JSON_FLOAT;

  targetTemperatureHandler.cb = actuate_Callback;
  targetTemperatureHandler.pKey = "target_temperature";
  targetTemperatureHandler.pData = &target_temperature;
  targetTemperatureHandler.type = SHADOW_JSON_FLOAT;

  heatingSystemStateHandler.cb = NULL;
  heatingSystemStateHandler.pKey = "heating_system_state";
  heatingSystemStateHandler.pData = &heating_system_state;
  heatingSystemStateHandler.type = SHADOW_JSON_STRING;

  getcwd(CurrentWD, sizeof(CurrentWD));
  
  Serial.print("Current WD: ");
  Serial.println(CurrentWD);

  Serial.print("Cert directory: ");
  Serial.println(certDirectory);

  snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
  snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
  snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

  Serial.print("rootCA: ");
  Serial.println(rootCA); 

  Serial.print("clientCRT: ");
  Serial.println(clientCRT);

  Serial.print("clientKey: ");
  Serial.println(clientKey);

  ShadowInitParameters_t sp = ShadowInitParametersDefault;
  sp.pHost = AWS_IOT_MQTT_HOST;
  sp.port = AWS_IOT_MQTT_PORT;
  sp.pClientCRT = clientCRT;
  sp.pClientKey = clientKey;
  sp.pRootCA = rootCA;
  sp.enableAutoReconnect = false;
  sp.disconnectHandler = NULL;

  Serial.println("Shadow Init");
  rc = aws_iot_shadow_init(&mqttClient, &sp);
  if(SUCCESS != rc) {
    Serial.println("Shadow Connection Error");
    return;
  }

  ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
  scp.pMyThingName = AWS_IOT_MY_THING_NAME;
  scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
  scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);

  Serial.println("Shadow Connect");
  rc = aws_iot_shadow_connect(&mqttClient, &scp);
  if(SUCCESS != rc) {
    Serial.println("Shadow Connection Error");
    return;
  }

  /*
   * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
   *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
   *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
   */
  rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
  if(SUCCESS != rc) {
    Serial.print("Unable to set Auto Reconnect to true - ");
    Serial.println(rc);
    return;
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &faucetActuator);
  if(SUCCESS != rc) {
    Serial.println("Shadow Register Faucet Delta Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &gasPriceHandler);
  if(SUCCESS != rc) {
    Serial.println("Shadow Gas Price Delta Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &electricPriceHandler);
  if(SUCCESS != rc) {
    Serial.println("Shadow Register Electric Delta Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &targetTemperatureHandler);
  if(SUCCESS != rc) {
    Serial.println("Shadow Register Target Temperature Delta Error");
  }
}

void main_loop() {
  /**
   * The main loop for local sensors and actuators
   */
  readFromSensors();
  
  while (readFromSerial()) {
    processCommand(command);
  }

  printInformationToLCD();

  debugStateMachine();
  state_machine();

  delay(100);
}

void loop() {
  // loop and publish a change in temperature
  if(NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc) {
    rc = aws_iot_shadow_yield(&mqttClient, 200);
    if(NETWORK_ATTEMPTING_RECONNECT == rc) {
      sleep(1);
      // If the client is attempting to reconnect we will skip the rest of the loop.
      return;
    }
    Serial.println("\n=======================================================================================\n");
    Serial.print("On Device: Faucet state ");
    Serial.println(isFaucetOn() ? "true" : "false");
    main_loop();

    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
    if(SUCCESS == rc) {
      rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 6, &temperatureHandler,
                       &faucetActuator, &gasPriceHandler, &electricPriceHandler, &targetTemperatureHandler, &heatingSystemStateHandler);
      if(SUCCESS == rc) {
        rc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
        if(SUCCESS == rc) {
          Serial.print("Update Shadow: ");
          Serial.println(JsonDocumentBuffer);
          rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                         ShadowUpdateStatusCallback, NULL, 4, true);
        }
      }
    }
    Serial.println("*****************************************************************************************\n");
    sleep(1);
  } else {
    if (MQTT_CONNECTION_ERROR == rc) {
      Serial.print("Error connecting to MQTT, ");
      Serial.println(rc);
    }
    else if(SUCCESS != rc) {
      Serial.print("An error occurred in the loop ");
      Serial.println(rc);
    }
  
    //Serial.println("Disconnecting");
    rc = aws_iot_shadow_disconnect(&mqttClient);
  
    if (NETWORK_DISCONNECTED_ERROR == rc) {
      Serial.print("Network disconnected error, ");
      Serial.println(rc);
    }
    else if(SUCCESS != rc) {
      Serial.print("Disconnect error ");
      Serial.println(rc);
    }
    
  }
}
